'use strict';
var pty = require('pty.js');

module.exports = function(codebox) {
  codebox.socket.service("termjs", function(socket) {
    var term = pty.fork(process.env.SHELL || 'sh', [], {
      name: require('fs').existsSync('/usr/share/terminfo/x/xterm-256color')
        ? 'xterm-256color'
        : 'xterm',
      cols: 80,
      rows: 24,
      cwd: process.env.HOME
    });

    term.on('data', function(data) {
        socket.write(JSON.stringify({
            "data":data
        }));
    });
    
    socket.on('data', function(data) {
        var message, i = 0;
        try {
            message = JSON.parse(data);
            data = message.data;
        } catch (err) {
            data = '';
        } finally {
            term.write(data);
        }
    });
    
    socket.on('close', function() {
        if (term) term.end();
        term = null;
        socket = null;
    });
    
    term.on('exit', function() {
        if (socket) socket.end();
        socket = null;
        term = null;
    });
    codebox.logger.log("termjs");
  });
};

