require("./stylesheets/main.less");
var TermJs = require("./views/termjs.js"),
    commands = codebox.require("core/commands");

commands.register({
    id: "termjs.open",
    title: "Terminal: Open",
    icon: "terminal",
    run: function(args, context) {
        codebox.tabs.setLayout(1); // Split horizontally
        codebox.tabs.add(TermJs, {}, {
            title: "Terminal",
            icon: "terminal",
            section: "terminal"
        });
      console.log("run termjs");
    }
});
