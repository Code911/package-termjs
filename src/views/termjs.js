var Terminal = require("../term.js"),
    Socket = codebox.require("core/socket"),
    _ = codebox.require("hr.utils"),
    $ = codebox.require("jquery");

var TerminalView = codebox.tabs.Panel.extend({
    className: "termjs",
    
    render: function() {
        var that = this;

        this.term = new Terminal({
            cols: 80,
            rows: 24,
            useStyle: true,
            screenKeys: true,
            cursorBlink: false
        });
        this.term.open(this.$el[0]);
        
        this.resizeHandler = _.bind(this.resize, this);
        
        // At this moment terminal has size 0, so we cannot determine the desired size
        // Give browser time to draw it and then resize
        setTimeout(this.resizeHandler, 200);
        
        $(window).resize(this.resizeHandler);
        this.on("tab:layout", this.resizeHandler);
        
        return TerminalView.__super__.render.apply(this, arguments);
    },
    
    finish: function() {
        var that = this;
        
        this.socket = new Socket({
            service: "termjs"
        });

        this.socket.once('open', function() {
            that.term.on("data", function(data) {
                if (data) {
                    that.socket.sock.send(JSON.stringify({"data":data}));
                }
            });

            that.socket.on("data", function(d) {
                if (d.data) {
                    that.term.write(d.data);
                }
            });

            that.socket.on('close', function() {
                that.closeTab();
            });                
        });
            
        return TerminalView.__super__.finish.apply(this, arguments);
    },
    
    remove: function() {
        $(window).unbind('resize', this.resizeHandler);
        this.off("tab:layout", this.resizeHandler);
        this.socket.sock.close();

        if (this.term) {
            this.term.destroy();
            this.term = null;
        }
        return TerminalView.__super__.remove.apply(this, arguments);
    },
    
    resize: function() {
        var term = this.$el.find(".terminal"),
            parent = this.$el.parent(),
            rows, cols;
            
        if (!this.term) {
            return;
        }

        // Scale terminal to fill entire area of parent element
        cols = parseInt((parent.width() * this.term.cols) / term.width());
        rows = parseInt((parent.height() * this.term.rows) / term.height());

        if (cols > 0 && rows > 0) {
            this.term.resize(cols, rows);
        }
        return this;
    }
});

module.exports = TerminalView;
