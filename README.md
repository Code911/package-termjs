# Webview

Connection local shell using term.js

## Installation

To install this addon in your codebox instance, run command:

    ./bin/codebox.js install -p termjs:git@bitbucket.org:Code911/package-termjs.git

Codebox command 'termjs.open' opens a new term.js terminal. Try it in
browser's JavaScript console:

    var commands = codebox.require("core/commands");
    commands.run("termjs.open");

To replace codebox built-in terminal with termjs, replace in settings
tree.toolbar command terminal.open with termjs.open. Either click on settings
and click Edit as JSON, or use sed from command line:

    sed -i -e 's/terminal.open/termjs.open/g' ~/.codebox/settings.json
